import {
  findCustomerCompanySegment,
  editCustomerCompanySegment
} from '../assets/api';

export default {
  namespaced: true,
  state: {
    data: {
      entities: {}
    }
  },
  mutations: {
    setCustomerCompanySegment(state, data) {
      state.data.entities[data.customerId] = data.data;
    }
  },
  actions: {
    load(context, customerId) {
      return findCustomerCompanySegment(customerId)
        .then((result) => {
          context.commit('setCustomerCompanySegment', {
            customerId,
            data: result
          });
        });
    },
    edit(context, {customerId, companyId, add}) {
      return editCustomerCompanySegment(customerId, companyId, add)
        .then((result) => {
          context.commit('setCustomerCompanySegment', {
            customerId,
            data: result
          });
        });
    }
  },
  getters: {
    getByCustomerId: (state) => (customerId) => {
      if (state.data.entities[customerId]) {
        return state.data.entities[customerId].entities;
      } else {
        return null;
      }
    }
  }
};