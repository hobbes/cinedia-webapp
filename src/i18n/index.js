import Vue from 'vue';
import i18next from 'i18next';
import VueI18Next from '@panter/vue-i18next';
import moment from 'moment';
import momentDurationFormat from 'moment-duration-format';

Vue.use(VueI18Next);

i18next.on('initialized', function(options) {
  moment.locale(options.lng);
});
i18next.on('languageChanged', function(lng) {
  moment.locale(lng);
});



i18next.init({
  lng: 'de',
  fallbackLng: ['de', 'dev'],
  resources: {
    de: {
      common: {
        logout: 'Logout'
      },
      home: {
        user: 'Benutzer',
        support: 'Support',
        customer: 'Kunde',
        name: 'Name',
        shortName: 'Kürzel',
        emailNotificationsDcpReady: 'Ich möchte eine E-Mail erhalten sobald ein DCP für einen Saal bereit ist.',
        type: {
          advertiser: 'Werbung',
          cinema: 'Kino'
        }
      },
      error: {
        dismiss: 'schliessen'
      },
      login: {
        email: 'E-Mail-Adresse',
        password: 'Passwort',
        password2x: 'Passwort wiederholen',
        emailOnTheWay: 'Die E-Mail ist unterwegs...',
        passwordAndEmail: {
          title: 'Login mit Passwort',
          description: 'Loggen Sie sich mit Ihrer bei omet.ch registrierten E-Mail-Adresse und Ihrem Passwort ein.',
          action: 'Login',
          change: 'Passwort ändern'
        },
        passwordless: {
          title: 'Login mit Direkt-Link',
          description: 'Loggen Sie sich einfach mit einem Klick auf einen Link ein, der an Ihre E-Mail-Adresse geschickt wird.',
          info: 'Klicken Sie auf den Link in der E-Mail um sich einzuloggen.',
          action: 'Link zuschicken'
        },
        passwordReset: {
          title: 'Passwort ändern',
          description: 'Sie erhalten zur Verifizierung eine E-Mail mit einem Link.',
          info: 'Klicken Sie auf den Link in der Email um ein neues Passwort einzugeben.',
          action: 'Link zuschicken'
        },
        passwordResetReturn: {
          title: 'Passwort wählen',
          description: 'Sie können hier Ihr neues Passwort wählen.',
          action: 'Passwort ändern',
          email: 'E-Mail-Adresse des Kontos',
          passwordNew: 'Neues Passwort',
          passwordNew2x: 'Neues Passwort wiederholen',
        },
        passwordlessReturn: {
          description: 'Sie werden eingeloggt und dann weiter geleitet...'
        },
        error: {
          userNotFound: {
            title: 'Benutzer nicht gefunden',
            message: 'Der Benutzer konnte nicht gefunden werden. Dieser wurde entweder noch nicht eingerichtet oder gelöscht.'
          },
          passwordInvalid: {
            title: 'Passwort ungültig',
            message: 'Das eingegebe Passwort ist ungültig.'
          },
          tokenInvalid: {
            title: 'Token ungültig',
            message: 'Entweder wurde der Link mit dem Token nicht ganz kopiert oder der Link wurde z.B. im E-Mail-Programm abgeschnitten.'
          },
          tokenExpired: {
            title: 'Token abgelaufen.',
            message: 'Der Token im Link ist nur eine eingeschränkte Zeit gültig und ist bereits abgelaufen.'
          },
          passwordNotSet: {
            title: 'Passwort nicht eingerichtet',
            message: 'Das Passwort dieses Benutzer wurde noch nicht eingerichtet.'
          },
          passwordDoNotMatch: {
            title: 'Passswörter stimmen nicht überein',
            message: 'Die beiden eingegebenen Passswörter stimmen nicht überein.'
          },
          userIsNotAssignedToCustomer: {
            title: 'Dieser Benutzer ist keinem Kunden zugeordnet.'
          }
        }
      },
      sujets: {
        search: 'Suche:',
        searchTerm: 'Suchebegriff:',
        searchAction: 'suchen',
        created: 'erstellt am: {{date, DD.MM.YY}}',
        nextDates: 'später',
        prevDates: 'früher',
        currDates: 'aktuell',
        delete: 'löschen',
        confirm: 'sicher? ',
        next: 'weiter',
        home: 'start',
        prev: 'zurück',
        bookingStateName: 'Buchungs-Status',
        bookingState: {
          current: 'aktuell gebucht',
          past: 'früher gebuchte',
          all: 'alle',
          new: 'nicht gebucht'
        },
        dias: 'Standbilder',
        spotsMute: 'Spots ohne Ton',
        spotsSound: 'Spots mit Ton',
        type: 'Sujet-Arten',
        typesName:'Sujet-Art',
        types: {
          dia: 'Standbild',
          spot_mute: 'Spot ohne Ton',
          spot_sound: 'Spot mit Ton'
        },
        filterAll: 'alle',
        duration: 'Dauer: {{duration}} Sekunden',
        durationShort: '{{duration}} Sekunden',
        sujetIsValidating: 'Die Daten werden überprüft...',
        sujetIsRendering: 'Die Daten sind OK. Sie können das Sujet nun buchen. Wir erstellen inzwischen die Bilder für die Vorschau im Webbrowser und bereiten die Daten für die Projektion im Kino vor.',
        reportIntermediateError: 'Fehler melden',
        sujetValidationErrorReported: 'Der Fehler wurde übermittelt. Wie versuchen das Bildmaterial manuell zu bearbeiten. Sie können das Sujet bereits buchen. Diese Buchungen sind aber noch nicht garantiert. Sie erhalten eine E-Mail sobald das Sujet bereit ist und wir die Buchungen bestätigen können.',
        intermediateError: 'Beim Vorbereiten der Daten für die Projektion ist ein Fehler aufgetreten. Wir wurden bereits darüber informiert und arbeiten daran den Fehler zu beheben. Sie können das Sujet weiterhin buchen.',
        noSujetsForCurrentFilterSettings: 'keine Sujets gefunden',
        resetFilter: 'Filter zurücksetzen',
        sujetEditTitle: 'bearbeiten',
        sujetEditTitleCancel: 'abbrechen',
        sujetEditTitleSave: 'speichern',
        error: {
          unknown: 'Das Bildmaterial dieses Sujets konnte nicht automatisch verarbeitet werden.', 
          audioVolumeOutOfRange: 'Der Tonpegel der Videodatei für einen Spot mit Ton ist entweder zu leise und daher nicht hörbar oder so laut, dass der Ton überschlägt.',
          unsupportedFiletype: 'Das Dateiformat wird nicht unterstützt.',
          unsupportedFramerate: 'Die Bildfrequenz wird nicht unterstützt.',
          missingAudiostream: 'In dieser Videodatei für einen Spot mit Ton wurde keine Tonspur gefunden.',
          sujetItemload: {
            title: 'Sujet nicht gefunden'
          },
          sujetCollectionLoad: {
            title: 'Sujets konnten nicht geladen werden'
          },
          conflict: {
            title: 'Konflikt beim Bearbeiten',
            message: 'Es gab einen Konflikt bei Bearbeiten dieses Sujets. Dieses wurde neu geladen um den aktuellen Stand zu zeigen.'
          }
        }
      },
      schedule: {
        kwDays: '{{date.startdate, DD.}} - {{date.enddate, DD.}}',
        kwStart: '{{date, DD.}}',
        kwEnd: '{{date, DD.}}',
        kwYear: '{{date, YYYY}}',
        kwMonth: '{{date, MMMM}}',
        theater: 'Saal',
        company: 'Firma',
        kw: 'Kinowoche',
        booked: 'gebucht',
        available: 'offen',
        error: {
          conflict: {
            title: 'Buchungs Konflikt',
            message: 'Es gab einen Konflikt bei der Buchung. Die Buchungen dieses Sujets wurden daher neu geladen um den aktuellen Stand zu zeigen.'
          }
        }
      },
      kw: 'Kinowoche',
      blocks: {
        prevWeek: 'vorherige Kinowoche',
        nextWeek: 'nächste Kinowoche',
        currWeek: 'aktuelle Kinowoche',
        lockedSince: 'Dieser Block wurde {{date, fromNow}}, am {{date, LLLL}} für Änderungen gesperrt.',
        lockedIn: 'Dieser Block wird {{date, fromNow}}, am {{date, LLLL}}, für Änderungen gesperrt.',
        dcpNotReady: 'DCP ist noch nicht bereit.',
        dcpReady: 'DCP ist bereit.',
        dcpDownload: 'Download',
        outdated: 'veraltet',
        notready: 'in Bearbeitung',
        duration: 'Dauer Block: {{seconds, duration}}',
        downloadLink: 'Download',
        dcp: 'DCP'

      },
      dcps: {
        lockdate: 'Lockzeit: {{date, dd HH}}h'
      },
      reel: {
        advertiser: 'Werbeanbieter',
        cinema: 'Kinobetrieb',
        sound: 'Segment mit Ton',
        mute: 'Segment ohne Ton',
        duration: 'Dauer: {{seconds, duration}}'
      },
      uploads: {
        dropFilesHere: 'oder Dateien in Browser ziehen',
        duration: 'Dauer',
        seconds: 'Sekunden',
        type: {
          image: 'Standbild',
          video: 'Spot'
        },
        sound: {
          sound: 'mit Ton',
          mute: 'ohne Ton'
        },
        cancel: 'Upload abbrechen',
        cancelAll: 'Alle Uploads abbrechen',
        upload: 'hochladen',
        uploadAll: 'Alle Dateien hochladen',
        remove: 'Upload entfernen',
        confirmRemove: 'sicher?',
        removeAll: 'Alle Uploads entfernen',
        error: {
          upload: 'Fehler beim Hochladen.',
          unsupportedUploadFiletype: '{{type}} Dateien werden nicht unterstützt.'
        },
        acceptedDataTypes: 'Folgende Daten können wir aktuell verarbeiten:',
        allowedImageTypes: 'Bilder: {{types}}',
        allowedVideoTypes: 'Videos: {{types}}'
      }
    },
    en: {
      sujets: {
        key1: 'one',
        key2: 'two'
      },
      schedule: {
        kwStart: '{{startdate, l}}'
      }
    },
  },
  interpolation: {
    format: function(value, format, lng) {
      if (moment(value).isValid() && format == 'fromNow') {
        return moment(value).fromNow();
      }
      if (format == 'duration') {
        return moment.duration(value, 's').format('mm:ss', {
          trim: false
        });
      }
      if (moment(value).isValid()) {
        return moment(value).format(format);
      }
      return value;
    }
  }
});


const i18n = new VueI18Next(i18next, {});

export default i18n;