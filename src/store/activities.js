import {
  activities
} from '../assets/api';
import generic from './generic';

export default {
  ...generic(activities)
};