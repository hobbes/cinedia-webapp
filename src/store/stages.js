import Vue from 'vue';
import forown from 'lodash.forown';
import {
  stages
} from '../assets/api';
import generic from './generic';

let stageStore = generic(stages);

stageStore.state.filter = {
  companies: {}
};

stageStore.mutations.setCollection = (state, collection) => {
  state.data = collection;

  collection.list.forEach(stageId => {
    let stage = collection.entities[stageId];

    if (state.filter.companies[stage.company.id] === undefined) {
      Vue.set(state.filter.companies, stage.company.id, true);
    }
  });
};
stageStore.mutations.setCompanyFilter = (state, {companyId, status}) => {
  state.filter.companies[companyId] = status;
};
stageStore.mutations.setAllCompanyFilters = (state, {status}) => {
  forown(state.filter.companies, (companyStatus, companyId) => {
    state.filter.companies[companyId] = status;
  });
};
stageStore.actions.changeCompanyFilter = (context, {companyId, status}) => {
  context.commit('setCompanyFilter', {companyId, status});
};
stageStore.actions.changeAllCompanyFilters = (context, {status}) => {
  context.commit('setAllCompanyFilters', {status});
};
stageStore.getters.listCompanies = (state, getters) => {
  let companyIds = {},
    list = [];

  getters.list.forEach(stage => {
    if (stage.company) {
      let compId = stage.company.id;

      if (!companyIds[compId]) {
        companyIds[compId] = compId;
        list.push(stage.company);
      }
    }
  });

  return list;
};
stageStore.getters.companyFilter = (state) => {
  return state.filter.companies;
};
stageStore.getters.filteredStages = (state, getters) => {
  return getters.list.filter((stage) => {
    return getters.companyFilter[stage.company.id] === true;
  });
};

export default {
  ...stageStore
};