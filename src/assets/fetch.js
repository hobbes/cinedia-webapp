import store from '../store/index';
import router from '../router/index';

function handleResponse(response) {
  store.commit('setFetchStatus', false);
  if (response.ok) {
    if (response.status !== 204) {
      return response.json();
    } else {
      return;
    }
  } else {
    if (response.status === 401) {
      store.dispatch('logout');
      router.push({
        path: '/login',
        query: {
          redirect: router.currentRoute.fullPath
        },
        hash: '#password'
      });
    }
    return response.json().then((errorResponse) => {
      let error = new Error(errorResponse.message);
  
      error.status = response.status;
      error.title = errorResponse.title;
      throw error;
    });
  }
}

export default function ({url, method='GET', body, headers}) {
  let jsonHeaders = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    options = {
      method: method,
      mode: 'cors',
      credentials: 'include',
      headers: headers || jsonHeaders
    };

  if (body) {
    options.body = JSON.stringify(body);
  }

  store.commit('setFetchStatus', true);
  return fetch(url, options)
    .then(handleResponse);
}