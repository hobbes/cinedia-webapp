import {
  getSegmentOrder,
  setSegmentOrder
} from '../assets/api';

export default {
  namespaced: true,
  state: {
    data: {
      entitiesMute: {},
      entitiesAudio: {}
    }
  },
  mutations: {
    setNewSegmentOrder(state, data) {
      if (data.hasAudio) {
        state.data.entitiesAudio[data.companyId] = data.data;
      } else {
        state.data.entitiesMute[data.companyId] = data.data;
      }
    }
  },
  actions: {
    load(context, {companyId, hasAudio}) {
      return getSegmentOrder(companyId, hasAudio)
        .then((result) => {
          context.commit('setNewSegmentOrder', {
            companyId,
            data: result,
            hasAudio
          });
        });
    },
    set(context, {companyId, hasAudio, order}) {
      return setSegmentOrder(companyId, hasAudio, order)
        .then((result) => {
          context.commit('setNewSegmentOrder', {
            companyId,
            data: result,
            hasAudio
          });
        });
    }
  },
  getters: {
    getMuteByCompanyId: (state) => (companyId) => {
      if (state.data.entitiesMute[companyId]) {
        return state.data.entitiesMute[companyId];
      } else {
        return null;
      }
    },
    getAudioByCompanyId: (state) => (companyId) => {
      if (state.data.entitiesAudio[companyId]) {
        return state.data.entitiesAudio[companyId];
      } else {
        return null;
      }
    }
  }
};