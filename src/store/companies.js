import {
  companies,
  // getSegmentOrder,
  // setSegmentOrder
} from '../assets/api';
import generic from './generic';

const companyStore = generic(companies);

// companyStore.mutations.setNewSegmentOrder = (state, data) => {
//   if (data.hasAudio) {
//     let newComp = state.data.entities[data.companyId];
//     newComp.segmentMute = data.data;
    
//     state.data.entities[data.companyId] = Object.assign({}, newComp)
//   } else {
//     let newComp = state.data.entities[data.companyId];
//     newComp.segmentAudio = data.data;
    
//     state.data.entities[data.companyId] = Object.assign({}, newComp)
//   }
// };

// companyStore.actions.loadSegmentOrder = (context, {companyId, hasAudio}) => {
//   return getSegmentOrder(companyId, hasAudio)
//     .then((result) => {
//       context.commit('setNewSegmentOrder', {
//         companyId,
//         data: result,
//         hasAudio
//       });
//     });
// };
// companyStore.actions.setSegmentOrder = (context, {companyId, hasAudio, order}) => {
//   return setSegmentOrder(companyId, hasAudio, order)
//     .then((result) => {
//       context.commit('setNewSegmentOrder', {
//         companyId,
//         data: result,
//         hasAudio
//       });
//     });
//   };

// companyStore.getters.getMuteSegmentOrderByCompanyId = (state) => (companyId) => {
//   if (state.data.entities[companyId] && state.data.entities[companyId].segmentMute) {
//     return state.data.entities[companyId].segmentMute;
//   } else {
//     return null;
//   }
// };
// companyStore.getters.getAudioSegmentOrderByCompanyId = (state) => (companyId) => {
//   if (state.data.entities[companyId] && state.data.entities[companyId].segmentAudio) {
//     return state.data.entities[companyId].segmentAudio;
//   } else {
//     return null;
//   }
// };

export default companyStore;