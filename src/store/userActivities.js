import {
  findUserActivities,
  editUserActivities
} from '../assets/api';

export default {
  namespaced: true,
  state: {
    data: {
      entities: {}
    }
  },
  mutations: {
    setUserActivities(state, data) {
      state.data.entities[data.userId] = data.data;
    }
  },
  actions: {
    load(context, userId) {
      return findUserActivities(userId)
        .then((result) => {
          context.commit('setUserActivities', {
            userId,
            data: result
          });
        });
    },
    edit(context, {userId, activityId, add}) {
      return editUserActivities(userId, activityId, add)
        .then((result) => {
          context.commit('setUserActivities', {
            userId,
            data: result
          });
        });
    }
  },
  getters: {
    getByUserId: (state) => (userId) => {
      if (state.data.entities[userId]) {
        return state.data.entities[userId].entities;
      } else {
        return null;
      }
    }
  }
};