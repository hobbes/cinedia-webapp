import {
  findCustomerReadAccess,
  editCustomerReadAccess
} from '../assets/api';

export default {
  namespaced: true,
  state: {
    data: {
      entities: {}
    }
  },
  mutations: {
    setCustomerReadAccess(state, data) {
      state.data.entities[data.customerId] = data.data;
    }
  },
  actions: {
    load(context, customerId) {
      return findCustomerReadAccess(customerId)
        .then((result) => {
          context.commit('setCustomerReadAccess', {
            customerId,
            data: result
          });
        });
    },
    edit(context, {customerId, customerReadAccessId, add}) {
      return editCustomerReadAccess(customerId, customerReadAccessId, add)
        .then((result) => {
          context.commit('setCustomerReadAccess', {
            customerId,
            data: result
          });
        });
    }
  },
  getters: {
    getByCustomerId: (state) => (customerId) => {
      if (state.data.entities[customerId]) {
        return state.data.entities[customerId].entities;
      } else {
        return null;
      }
    }
  }
};