import moment from 'moment';
import {loadDateslotCollection} from '../assets/api';

function defaultStartdate() {
  return moment().subtract(1, 'week').day(4);
}

function init() {
  return {
    uiStartdate: defaultStartdate(),
    uiWeekrange: 8,
    data: {
      list: [],
      entities: {}
    }
  };
}

export default {
  state: init(),
  mutations: {
    setDateslotCollection (state, data) {
      state.data = data;
    },
    setUiStartdate (state, startdate) {
      state.uiStartdate = startdate;
    },
    setUiWeekrange (state, weekrange) {
      state.uiWeekrange = weekrange;
    },
    reset (state) {
      state = init();
    },
    resetUiStartdate(state) {
      state.uiStartdate = defaultStartdate();
    }
  },
  actions: {
    loadDateslotCollection(context, {startdate, enddate}) {
      return loadDateslotCollection(startdate.format('YYYYMMDD'), enddate.format('YYYYMMDD'))
        .then((result) => {
          context.commit('setDateslotCollection', result);
        });
    }
  },
  getters: {
    getUiStartdate(state) {
      return moment(state.uiStartdate);
    },
    getUiEnddate(state) {
      return moment(state.uiStartdate).add(state.uiWeekrange, 'weeks');
    },
    getUiWeekrange(state) {
      return state.uiWeekrange;
    },
    listDates (state, getters) {
      return state.data.list.map((dateslotId) => {
        return state.data.entities[dateslotId];
      });
    }
  }
};