import Vue from 'vue';
import {
  loadScheduleCollection,
  toggleDateslotbooking
} from '../assets/api';


export default {
  state: {
    data: {},
    ui: {}
  },
  mutations: {
    setScheduleCollection (state, {sujetId, data}) {
      let newSchedule = {};

      newSchedule[sujetId] = data;
      state.data = Object.assign({}, state.data, newSchedule);
    },
    togglePending (state, {sujetId, stageId, dateslotIds, status}) {
      Object.keys(dateslotIds).forEach((stageId) => {
        dateslotIds[stageId].forEach((dateslotId) => {
          
          if (!state.ui[sujetId]) {
            Vue.set(state.ui, sujetId, {});
          }
          if (!state.ui[sujetId][stageId]) {
            Vue.set(state.ui[sujetId], stageId, {});
          }
          if (!state.ui[sujetId][stageId][dateslotId]) {
            Vue.set(state.ui[sujetId][stageId], dateslotId, {});
          }
          Vue.set(state.ui[sujetId][stageId][dateslotId], 'pending', status);
        });
      });
    },
    reset (state) {
      Object.getOwnPropertyNames(state.data).forEach(
        (val, idx, array) => {
          delete state.data[val];
        }
      );
    }
  },
  actions: {
    loadScheduleCollection(context, {sujetId, startdate, enddate}){
      return loadScheduleCollection(sujetId, {
        startdate: startdate.format('YYYYMMDD'),
        enddate: enddate.format('YYYYMMDD')
      }).then((result) => {
        context.commit('setScheduleCollection', {
          sujetId,
          data: result
        });
      });
    },
    toggleDateslotbooking(context, data) {
      context.commit('togglePending', {
        ...data,
        status: true
      });
      return toggleDateslotbooking(data)
        .then((result) => {
          context.commit('setScheduleCollection', {
            sujetId: data.sujetId,
            data: result
          });
          context.commit('togglePending', {
            ...data,
            status: false
          });
        })
        .catch((error) => {
          context.commit('togglePending', {
            ...data,
            status: false
          });
          throw error;
        });
    }
  },
  getters: {
    getScheduleBySujetId: (state) => (sujetId) => {
      if (state.data[sujetId]) {
        return state.data[sujetId];
      } else {
        return {
          'list': [],
          'entities': {}
        };
      }
    },
    getScheduleUiBySujetId: (state) => (sujetId) => {
      if (state.ui[sujetId]) {
        return state.ui[sujetId];
      } else {
        return {};
      }
    }
  }
};