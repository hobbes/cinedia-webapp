import {
  loadDcpCollection,
  renderBlockDcp
} from '../assets/api';

import moment from 'moment';

export default {
  state: {
    view: {},
    data: {
      list: [],
      entities: {}
    }
  },
  mutations: {
    setBlockQueryParams(state, query) {
      state.view = query;
    },
    setDcpCollection (state, data) {
      state.data = data;
    },
    setDcpItem (state, block) {
      let updatedDcp = {};

      updatedDcp[block.id] = block;
      state.data.entities = Object.assign({}, state.data.entities, updatedDcp);
    },
    reset(state) {
      state.view = {};
      state.data = {
        list: [],
        entities: {}
      };
    }
  },
  actions: {
    loadDcpCollection(context, query) {
      return loadDcpCollection(query.date)
        .then((result) => {
          context.commit('setDcpCollection', result);
        });
    },
    renderBlockDcp(context, blockid) {
      return renderBlockDcp(blockid)
        .then((result) => {
          context.commit('setDcpItem', result);
        });
    }
  },
  getters: {
    currentDcpPageLink: (state) => {
      let date = '';
      if (!state.view.date) {
        date = moment().day() >= 4 ? moment().day(11).format('YYYYMMDD') : moment().day(4).format('YYYYMMDD');
      }
      return {
        name: 'DcpCollection',
        query: {
          date,
          ...state.view
        }
      };
    },
    newDcpDateLink: (state) => (date) => {
      return {
        name: 'DcpCollection',
        query: {
          ...state.view,
          date
        }
      };
    },
    listdcps: (state) => {
      return state.data.list.map(id => {
        return state.data.entities[id];
      });
    }
  }
};