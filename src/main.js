// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import store from './store';
import router from './router';
import i18n from './i18n';

Vue.config.productionTip = false;

window.onbeforeunload = function() {
  if (store.getters.pendingUploads) {
    return 'Two buttons';
  }
  return null;
};

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: {
    App
  }
});