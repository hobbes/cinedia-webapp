import {
  locations
} from '../assets/api';
import generic from './generic';

export default {
  ...generic(locations)
};