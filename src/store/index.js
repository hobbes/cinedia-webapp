import Vue from 'vue';
import Vuex from 'vuex';

import sujets from './sujets';
import schedule from './schedule';
import blocks from './blocks';
import dcps from './dcps';
import users from './users';
import userActivities from './userActivities';
import customers from './customers';
import customerReadAccess from './customerReadAccess';
import customerCompanySegment from './customerCompanySegment';
import customerReadCompanies from './customerReadCompanies';
import segments from './segments';
import dateslot from './dateslot';
import stages from './stages';
import companies from './companies';
import companySegmentOrder from './companySegmentOrder';
import locations from './locations';
import activities from './activities';
import uploads from './uploads';

import {
  checkAuth,
  pwdLogin,
  logout,
  getPasswordlessLogintoken,
  passwordlessLogin,
  passwordReset,
  passwordResetReturn
} from '../assets/api';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    user: false,
    pendigFetch: false,
    error: {
      show: false,
      statusCode: 0,
      title: '',
      message: ''
    }
  },
  mutations: {
    showError (state, error) {
      state.error.show = true;
      state.error.title = error.title;
      state.error.message = error.message;
      state.error.statusCode = error.statusCode;
    },
    hideError (state) {
      state.error.show = false;
      state.error.title = '';
      state.error.message = '';
      state.error.statusCode = 0;
    },
    setFetchStatus (state, status) {
      state.pendigFetch = status;
    },
    login (state, user) {
      state.user = user;
    },
    logout (state) {
      state.user = false;
    }
  },
  actions: {
    showError(context, error) {
      context.commit('showError', error);
    },
    hideError(context) {
      context.commit('hideError');
    },
    checkAuth(context, payload) {
      return checkAuth();
    },
    logout(context) {
      return logout()
        .then(() => {
          context.commit('logout');
          context.commit('reset');
          context.commit('stages/reset');
          context.commit('customers/reset');
          context.commit('locations/reset');
          context.commit('users/reset');
        });
    },
    passwordLogin(context, {email, password}) {
      return pwdLogin({email, password})
        .then((user) => {
          context.commit('login', user);
        });
    },
    getPasswordlessLogintoken(context, payload) {
      return getPasswordlessLogintoken(payload);
    },
    passwordlessLogin(context, payload) {
      return passwordlessLogin(payload)
        .then((user) => {
          context.commit('login', user);
        });
    },
    passwordReset(context, payload) {
      return passwordReset(payload);
    },
    passwordResetReturn(context, payload) {
      return passwordResetReturn(payload)
        .then((user) => {
          context.commit('login', user);
        });
    }
  },
  getters: {
    error: (state) => {
      return state.error;
    },
    getFetchStatus: (state) => {
      return state.pendigFetch;
    },
    getUser: (state) => {
      if (state.user) {
        return state.user;
      }
      return null;
    },
    userHasActivity: (state) => (activity) => {
      if (state.user) {
        return state.user.activities.indexOf(activity) !== -1;
      } else {
        return false;
      }
    },
    userIsAssignedToCustomer: (state) => {
      if (state.user && state.user.customer.id) {
        return true;
      } else {
        return false;
      }
    },
    userHasAssignedBookingStages: (state) => {
      if (state.user &&
            state.user.access &&
            state.user.access.stages &&
            state.user.access.stages.write &&
            state.user.access.stages.write.length > 0) {
        return true;
      } else {
        return false;
      }
    },
    userIsAdmin: (state) => {
      if (state.user) {
        return state.user.activities.indexOf('admin') !== -1 ||
          !state.user.customer.id;
      } else {
        return false;
      }
    }
  },
  modules: {
    sujets,
    blocks,
    dcps,
    users,
    userActivities,
    customers,
    customerReadAccess,
    customerCompanySegment,
    customerReadCompanies,
    schedule,
    segments,
    dateslot,
    stages,
    locations,
    companies,
    companySegmentOrder,
    uploads,
    activities
  }
});