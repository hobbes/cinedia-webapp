import Vue from 'vue';
import throttle from 'lodash.throttle';
import {
  loadSujetCollection,
  loadSujetItem,
  updateSujetName,
  deleteSujetItem,
  reportSujetValidationError,
  replaceSourceFile,
  uploadFile,
  getUploadUrl,
  eventSourceSujetProgressUrl
} from '../assets/api';

const sse = {};
const throttledNameUpdate = throttle((context, data) => {
  return updateSujetName(data)
    .then((sujetItem) => {
      addSSE(context, sujetItem);
      context.commit('setSujetItem', sujetItem);
    });
}, 1000);


function addSSE(context, sujet) {
  // if (context.state.data.entities[sujet.id]) {
  //   sujet.sourcefile.validation.job.progress = context.state.data.entities[sujet.id].sourcefile.validation.job.progress;
  //   sujet.intermediate.job.progress = context.state.data.entities[sujet.id].intermediate.job.progress;
  // } else {
  //   sujet.sourcefile.validation.job.progress = 0;
  //   sujet.intermediate.job.progress = 0;
  // }
  
  if (sse[sujet.id] && sujet.intermediate.ready && sujet.sourcefile.validated) {
    sse[sujet.id].close();
    delete sse[sujet.id];
  }

  if (!!window.EventSource && (sujet.intermediate.job.id || sujet.sourcefile.validation.job.id)) {

    if (!sse[sujet.id]) {
      sse[sujet.id] = new EventSource(eventSourceSujetProgressUrl(sujet.id), {
        withCredentials: true
      });

      sse[sujet.id].addEventListener('sourcefilevalidation', (e) => {
        let progress = Number(e.data);

        context.commit('setSujetItemSourcefileValidationProgress', {
          sujetId: sujet.id,
          progress
        });
      }, false);

      sse[sujet.id].addEventListener('sourcefilevalidation.finish', () => {
        context.commit('setSujetItemSourcefileValidationProgress', {
          sujetId: sujet.id,
          progress: 100
        });
        sse[sujet.id].close();
        delete sse[sujet.id];
        context.dispatch('loadSujetItem', sujet.id);
      }, false);

      sse[sujet.id].addEventListener('intermediate', (e) => {
        let progress = Number(e.data);

        context.commit('setSujetItemIntermediatenProgress', {
          sujetId: sujet.id,
          progress
        });
      }, false);

      sse[sujet.id].addEventListener('intermediate.finish', () => {
        context.commit('setSujetItemIntermediatenProgress', {
          sujetId: sujet.id,
          progress: 100
        });
        sse[sujet.id].close();
        delete sse[sujet.id];
        context.dispatch('loadSujetItem', sujet.id);
      }, false);


    }
  }
}

export default {
  state: {
    view: {},
    data: {
      list: [],
      entities: {}
    }
  },
  mutations: {
    setQueryParams(state, query) {
      state.view = query;
    },
    setSujetCollection (state, data) {
      state.data = data;
    },
    setSujetItem (state, sujet) {
      let updateSujet = {};

      updateSujet[sujet.id] = sujet;
      state.data.entities = Object.assign({}, state.data.entities, updateSujet);
    },
    setSujetItemSourcefileValidationProgress (state, {sujetId, progress}) {
      state.data.entities[sujetId].sourcefile.validation.job.progress = progress;
    },
    setSujetItemIntermediatenProgress (state, {sujetId, progress}) {
      state.data.entities[sujetId].intermediate.job.progress = progress;
    },
    setSujetUploadRequest(state, {request, sujetId}) {
      Vue.set(state.data.entities[sujetId], 'request', {
        request: request,
        progress: 0,
        error: false
      });
    },
    setSujetUploadProgress(state, {progress, sujetId}) {
      if (state.data.entities[sujetId].request) {
        state.data.entities[sujetId].request.progress = progress;
      }
    },
    setSujetUploadError(state, {error, sujetId}) {
      if (state.data.entities[sujetId].request) {
        state.data.entities[sujetId].request.error = error;
      }
    },
    reset(state) {
      state.view = {};
      state.data = {
        list: [],
        entities: {}
      };
    }
  },
  actions: {
    loadSujetCollection(context, query){
      return loadSujetCollection(query)
        .then((sujetCollection) => {
          sujetCollection.list.forEach((sujetId) => {
            addSSE(context, sujetCollection.entities[sujetId]);
          });
          context.commit('setSujetCollection', sujetCollection);
        });
    },
    loadSujetItem(context, sujetId) {
      return loadSujetItem(sujetId)
        .then((sujetItem) => {
          addSSE(context, sujetItem);
          context.commit('setSujetItem', sujetItem);
        });
    },
    updateSujetName(context, data) {
      return throttledNameUpdate(context, data);
    },
    deleteSujetItem(context, data) {
      return deleteSujetItem(data);
    },
    reportSujetValidationError(context, sujetId) {
      return reportSujetValidationError(sujetId)
        .then((result) => {
          context.commit('setSujetItem', result);
        });
    },
    uploadSourcefile(context, {file, sujetId}) {
      getUploadUrl(file.name)
        .then(({key, url, contentType}) => {
          let request = uploadFile({
            url,
            file,
            contentType
          }, function progress(progress) {
            context.commit('setSujetUploadProgress', {
              sujetId,
              progress
            });
          }, function finished(error){
            if (error) {
              context.commit('setSujetUploadError', {
                sujetId,
                error
              });
              return;
            } else {
              return replaceSourceFile({
                sujetId,
                filekey: key
              }).then((result) => {
                context.commit('setSujetItem', result);
              });
            }
          });
          context.commit('setSujetUploadRequest', {
            sujetId,
            request
          });
        })
        .catch((error) => {
          context.commit('setSujetUploadError', {
            sujetId,
            error
          });
        });
    }
  },
  getters: {
    list: (state) => {
      return state.data.list.map(id => {
        return state.data.entities[id];
      });
    },
    getSujetById: (state) => (sujetId) => {
      if (state.data.entities[sujetId]) {
        return state.data.entities[sujetId];
      } else {
        return null;
      }
    },
    currentPageLink: (state) => {
      return {
        name: 'SujetList',
        query: {
          status: 'all',
          types: ['dia', 'spotmute', 'spotsound'],
          ...state.view,
        }
      };
    },
    currentPageClearSearchLink: (state) => {
      let view = {
        name: 'SujetList',
        query: {
          ...state.view,
        }
      };

      delete view.query.search;
      return view;
    },
    getSujetStatusLink: (state) => (sujsetStatus) => {
      let view = {
        name: 'SujetList',
        query: {
          ...state.view,
          status: sujsetStatus
        }
      };
      delete view.query.pointer;
      delete view.query.direction;
      return view;
    },
    getSearchLink: (state) => (searchString) => {
      let trimmedSearchString = searchString.trim(),
        view = {
          name: 'SujetList',
          query: {
            ...state.view,
            search: trimmedSearchString
          }
        };
      delete view.query.pointer;
      delete view.query.direction;

      if (!trimmedSearchString) {
        delete view.query.search;
      }
      return view;
    },
    getSujetTypeLink: (state) => (typeState) => {
      let view = {
        name: 'SujetList',
        query: {
          ...state.view
        }
      };

      delete view.query.pointer;
      delete view.query.direction;

      if (!view.query.types) {
        view.query.types = typeState;
      } else {
        if (Array.isArray(view.query.types)) {
          if (view.query.types.includes(typeState)) {
            view.query.types = view.query.types.filter(item => item !== typeState);
            if (view.query.types.length === 0) {
              delete view.query.types;
            }
          } else {
            let newArray = view.query.types.slice();
            newArray.push(typeState);
            
            view.query.types = newArray;
          }
        } else {
          if (view.query.types === typeState) {
            delete view.query.types;
          } else {
            view.query.types = [view.query.types, typeState];
          }
        }
      }

      return view;
    },
    nextPageLink: (state) => {
      return {
        name: 'SujetList',
        query: {
          ...state.view,
          pointer: state.data.list[state.data.list.length - 1],
          direction: 'next'
        }
      };
    },
    homePageLink: (state) => {
      let view = {
        name: 'SujetList',
        query: {
          ...state.view
        }
      };

      delete view.query.pointer;
      delete view.query.direction;

      return view;
    },
    prevPageLink: (state) => {
      return {
        name: 'SujetList',
        query: {
          ...state.view,
          pointer: state.data.list[0],
          direction: 'prev'
        }
      };
    },
    resetLink: (state) => {
      return {
        name: 'SujetList',
        query: {
          status: 'new',
          types: ['dia', 'spotmute', 'spotsound'],
        }
      };
    },
    progressUrl: (state) => (sujetId) => {
      return eventSourceSujetProgressUrl(sujetId);
    }
  }
};