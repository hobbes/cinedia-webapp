import {
  loadBlockCollection,
  loadBlockItem
} from '../assets/api';

import moment from 'moment';

function setCurrentWeek() {
  if (moment().day() >= 4) {
    return moment().day(11);
  } else {
    return moment().day(4);
  }
}

export default {
  state: {
    view: {},
    data: {
      list: [],
      entities: {}
    }
  },
  mutations: {
    setBlockQueryParams(state, query) {
      state.view = query;
    },
    setBlockCollection (state, data) {
      state.data = data;
    },
    setBlockItem (state, block) {
      let updatedBlock = {};

      updatedBlock[block.id] = block;
      state.data.entities = Object.assign({}, state.data.entities, updatedBlock);
    },
    reset(state) {
      state.view = {};
      state.data = {
        list: [],
        entities: {}
      };
    }
  },
  actions: {
    loadBlockCollection(context, query){
      return loadBlockCollection(query.date)
        .then((result) => {
          context.commit('setBlockCollection', result);
        });
    },
    loadBlockItem(context, blockId) {
      return loadBlockItem(blockId)
        .then((result) => {
          context.commit('setBlockItem', result);
        });
    },
  },
  getters: {
    currentBlockPageLink: (state) => {
      return {
        name: 'BlockCollection',
        query: {
          ...state.view,
          date: setCurrentWeek().format('YYYYMMDD')
        }
      };
    },
    prevBlockPageLink: (state) => {
      return {
        name: 'BlockCollection',
        query: {
          ...state.view,
          date: moment(state.view.date).subtract(1, 'week').format('YYYYMMDD')
        }
      };
    },
    nextBlockPageLink: (state) => {
      return {
        name: 'BlockCollection',
        query: {
          ...state.view,
          date: moment(state.view.date).add(1, 'week').format('YYYYMMDD')
        }
      };
    },
    newDateLink: (state) => (date) => {
      return {
        name: 'BlockCollection',
        query: {
          ...state.view,
          date
        }
      };
    },
    listblocks: (state) => {
      return state.data.list.map(id => {
        return state.data.entities[id];
      });
    },
    getBlockById: (state) => (blockId) => {
      if (state.data.entities[blockId]) {
        return state.data.entities[blockId];
      } else {
        return {
          id: 'ID',
          name: 'KW00-00-00',
          hash: '',
          locked: false,
          stage: {
            id: 'ID',
            name: 'name',
            nameshort: 'nameshort'
          },
          location: {
            id: 'ID',
            name: 'name'
          },
          company: {
            id: 'ID',
            name: 'name',
            nameshort: 'nameshort'
          },
          dateslot: {
            id: 'ID',
            kw: 37,
            startdate: new Date(),
            enddate: new Date()
          },
          dcp: {
            hash: null,
            href: '',
            pending: false,
            error: false,
            ready: false
          },
          customers: {}
        };
      }
    }
  }
};