# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [6.1.0] - 2018-09-23
- Support for png images. Filetype is availabel to upload.
- Display error message if filetype of upload is unsupported.

## [6.0.0] - 2018-09-17
### Added
- Initial deployment for betatest.