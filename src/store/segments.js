import {
  loadSegmentCollection,
  updateReelOrder
} from '../assets/api';

export default {
  state: {
    data: {}
  },
  mutations: {
    setSegmentCollection (state, {blockId, data}) {
      let newSegment = {};

      newSegment[blockId] = data;
      state.data = Object.assign({}, state.data, newSegment);
    },
    setSujetListOrder (state, {blockId, segmentId, list}) {
      state.data[blockId].entities[segmentId].sujets = list;
    },
    reset (state) {
      Object.getOwnPropertyNames(state.data).forEach(
        (val, id, array) => {
          delete state.data[val];
        }
      );
    }
  },
  actions: {
    loadSegmentCollection(context, blockId){
      return loadSegmentCollection(blockId)
        .then((result) => {
          context.commit('setSegmentCollection', {
            blockId,
            data: result
          });
        });
    },
    setNewOrder(context, {blockId, segmentId, list, hash}) {
      let oldList = context.state.data[blockId].entities[segmentId].sujets,
        idArray = list.map((item) => {
          return item.id;
        });

      context.commit('setSujetListOrder', {blockId, segmentId, list});
      return updateReelOrder({blockId, segmentId, list: idArray, hash})
        .then((result) => {
          context.commit('setSegmentCollection', {
            blockId, 
            data: result});
        })
        .catch((error) => {
          context.commit('setSujetListOrder', {blockId, segmentId, list: oldList});
          throw error;
        });
    }
  },
  getters: {
    getSegmentsByBlockId: (state) => (blockId) => {
      if (state.data[blockId]) {
        return state.data[blockId].list.map((segmentId) => {
          return state.data[blockId].entities[segmentId];
        });
      } else {
        return [];
      }
    }
  }
};