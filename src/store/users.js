import {
  users,
  sendUserWelcomeMail
} from '../assets/api';
import generic from './generic';

const userStore = generic(users);

userStore.actions.sendWelcomeMail = function (context, userId) {
  return sendUserWelcomeMail(userId);
};

export default userStore;