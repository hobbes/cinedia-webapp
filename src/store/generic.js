import Vue from 'vue';

export default function ({
  create,
  find,
  get,
  update,
  del
}) {
  return {
    namespaced: true,
    state: {
      data: {
        list: [],
        entities: {}
      }
    },
    mutations: {
      setCollection (state, collection) {
        state.data = collection;
      },
      setItem (state, {id, item}) {
        Vue.set(state.data.entities, id, item);
      },
      addItem (state, item) {
        state.data.entities[item.id] = item;
        state.data.list.push(item.id);
      },
      reset(state) {
        state.data = {
          list: [],
          entities: {}
        };
      }
    },
    actions: {
      loadCollection(context){
        return find()
          .then((result) => {
            context.commit('setCollection', result);
          });
      },
      loadItem(context, itemId){
        return get(itemId)
          .then((fetchedItem) => {
            context.commit('setItem', {
              id: itemId,
              item: fetchedItem
            });
          });
      },
      create(context, data) {
        return create(data)
          .then((item) => {
            context.commit('addItem', item);
            return item;
          });
      },
      update(context, {id, path, value}) {
        let patch = [{
          op: 'replace',
          path,
          value
        }];

        return update({
          id,
          body: patch
        }).then((updatedItem) => {
          context.commit('setItem', {
            id,
            item: updatedItem
          });
        });
      },
      delete(context, data) {
        return del(data)
          .then(() => {
            context.dispatch('loadCollection');
          });
      },
      reset(state) {
        state.view = {};
        state.data = {
          list: [],
          entities: {}
        };
      }
    },
    getters: {
      list: (state) => {
        return state.data.list.map(itemId => {
          return state.data.entities[itemId];
        });
      },
      getById: (state) => (itemId) => {
        if (state.data.entities[itemId]) {
          return state.data.entities[itemId];
        } else {
          return null;
        }
      }
    }
  };
}