import Vue from 'vue';
import Router from 'vue-router';
import ViewHome from '../views/Home';
import ViewLogin from '../views/Login';
import ViewSujetList from '../views/SujetList';
import ViewSujetUpload from '../views/SujetUpload';
import ViewSujetItem from '../views/SujetItem';
import ViewBlockCollection from '../views/BlockCollection';
import ViewBlockItem from '../views/BlockItem';
import ViewAdmin from '../views/Admin';
import ViewDcpCollection from '../views/DcpCollection';
import ViewUsers from '../views/Users';
import ViewCustomers from '../views/Customers';
import ViewStages from '../views/Stages';
import ViewNotFound from '../views/NotFound';

import store from '../store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  fallback: false,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      const position = {};

      if (to.hash) {
        position.selector = to.hash;
      } else {
        position.x = 0;
        position.y = 0;
      }

      return position;
    }
  },
  routes: [
    {
      path: '/',
      redirect: {
        name: 'Home'
      }
    }, {
      path: '/login',
      name: 'Login',
      component: ViewLogin,
      props: (route) => {
        return {
          query: route.query,
          hash: route.hash
        };
      }
    }, {
      path: '/home',
      name: 'Home',
      component: ViewHome,
      props: true,
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/uploads',
      name: 'SujetUpload',
      component: ViewSujetUpload,
      props: true,
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/sujets',
      name: 'SujetList',
      component: ViewSujetList,
      props: (route) => {
        return {
          query: route.query
        };
      },
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/sujets/:sujetid',
      name: 'SujetItem',
      component: ViewSujetItem,
      props: (route) => {
        return {
          sujetid: route.params.sujetid,
          stageid: route.query.stage,
          dateslotid: route.query.dateslot
        };
      },
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/blocks',
      name: 'BlockCollection',
      component: ViewBlockCollection,
      props: (route) => {
        return {
          query: route.query
        };
      },
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/blocks/:blockid',
      name: 'BlockItem',
      component: ViewBlockItem,
      props: true,
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/admin',
      name: 'Admin',
      component: ViewAdmin,
      props: true,
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/admin/dcps',
      name: 'DcpCollection',
      component: ViewDcpCollection,
      props: (route) => {
        return {
          query: route.query
        };
      },
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/admin/users',
      name: 'Users',
      component: ViewUsers,
      props: true,
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/admin/customers',
      name: 'Customers',
      component: ViewCustomers,
      props: true,
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/admin/stages',
      name: 'Stages',
      component: ViewStages,
      props: true,
      meta: {
        requiresAuth: true
      }
    }, { 
      path: '*',
      name: 'NotFound',
      component: ViewNotFound,
      props: true,
      meta: {
        requiresAuth: false
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  store.dispatch('hideError');
  if (to.matched.some(route => route.meta.requiresAuth)) {
    store.dispatch('checkAuth')
      .then(function (user) {
        store.commit('login', user);
        next();
      })
      .catch(function(err) { 
        store.dispatch('logout');
        next({
          path: '/login',
          query: {
            redirect: to.fullPath
          },
          hash: '#password'
        });
      });
  } else {
    next();
  }
});

export default router;