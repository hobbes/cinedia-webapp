import {
  findCustomerReadCompanies,
  editCustomerReadCompany
} from '../assets/api';

export default {
  namespaced: true,
  state: {
    data: {
      entities: {}
    }
  },
  mutations: {
    setCustomerReadCompanies(state, data) {
      state.data.entities[data.customerId] = data.data;
    }
  },
  actions: {
    load(context, customerId) {
      return findCustomerReadCompanies(customerId)
        .then((result) => {
          context.commit('setCustomerReadCompanies', {
            customerId,
            data: result
          });
        });
    },
    edit(context, {customerId, companyId, add}) {
      return editCustomerReadCompany(customerId, companyId, add)
        .then((result) => {
          context.commit('setCustomerReadCompanies', {
            customerId,
            data: result
          });
        });
    }
  },
  getters: {
    getByCustomerId: (state) => (customerId) => {
      if (state.data.entities[customerId]) {
        return state.data.entities[customerId].entities;
      } else {
        return null;
      }
    }
  }
};