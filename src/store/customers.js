import {
  customers
} from '../assets/api';
import generic from './generic';

export default {
  ...generic(customers)
};