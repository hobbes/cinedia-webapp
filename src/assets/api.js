import qs from 'qs';
import request from './fetch';

const HOST = __APICINEDIA__;
const APPHOST = __APPCINEDIA__;

const CHECK_AUTH_URL = `${HOST}/auth/check`,
  LOGOUT_URL = `${HOST}/auth/logout`,
  PASSWORD_LOGIN_URL = `${HOST}/auth/pwd`,
  PASSWORDLESS_LOGIN_URL = `${HOST}/auth/nopwd`,
  PASSWORDLESS_LOGIN_RETURN_URL = `${HOST}/auth/nopwd/return`,
  PASSWORDRESET_URL = `${HOST}/auth/pwd/reset`,
  PASSWORDRESET_RETURN_URL= `${HOST}/auth/pwd/reset/return`;

function generic(resourceName) {
  return {
    create(newItem){
      return request ({
        url: `${HOST}/${resourceName}`,
        method: 'POST',
        body: newItem
      });
    },
    find(){
      return request ({
        url: `${HOST}/${resourceName}`
      });
    },
    get(itemId){
      return request ({
        url: `${HOST}/${resourceName}/${itemId}`
      });
    },
    update(data){
      return request ({
        url: `${HOST}/${resourceName}/${data.id}`,
        method: 'PATCH',
        body: data.body
      });
    },
    del(data){
      return request ({
        url: `${HOST}/${resourceName}/${data.id}`,
        method: 'DELETE',
        body: data.body
      });
    }
  };
}

export const adminQueueUrl = `${HOST}/adminqueue`;

export function eventSourceSujetProgressUrl(sujetId) {
  return `${HOST}/sujets/${sujetId}/progress`;
}

export function uploadFile({url, file, contentType}, progress, finished) {
  const xhr = new XMLHttpRequest();
  let lastProgress = 0;

  function reportProgress(event) {
    if (event.lengthComputable) {
      let currentProgress = ((event.loaded / event.total) * 100).toFixed();
      if (currentProgress !== lastProgress) {
        lastProgress = currentProgress;
        progress(currentProgress);
      }
    }
  }

  xhr.open('PUT', url, true);
  xhr.setRequestHeader('Content-Type', contentType);

  xhr.upload.onabort = function () {
    finished(new Error('upload.error.aborted'));
  };
  xhr.upload.onerror = function () {
    finished(new Error('upload.error'));
  };
  xhr.onreadystatechange = function() {
    if (this.readyState === 4 && this.status !== 0) {
      finished();
    }
  };
  xhr.upload.onloadstart = reportProgress;
  xhr.upload.onprogress = reportProgress;
  xhr.send(file);

  return {
    abort() {
      xhr.abort();
    }
  };
}

export function getUploadUrl(filename) {
  return request ({
    url: `${HOST}/storage/uploads`,
    method: 'POST',
    body: {
      filename
    }
  });
}

export function createSujet(body) {
  return request ({
    url: `${HOST}/sujets`,
    method: 'POST',
    body
  });
}

export function checkAuth() {
  return request ({
    url: CHECK_AUTH_URL,
    method: 'POST'
  });
}

export function logout() {
  return request ({
    url: LOGOUT_URL,
    method: 'POST'
  });
}

export function pwdLogin(payload) {
  return request ({
    url: PASSWORD_LOGIN_URL,
    method: 'POST',
    body: payload
  });
}

export function getPasswordlessLogintoken(payload) {
  return request ({
    url: PASSWORDLESS_LOGIN_URL,
    method: 'POST',
    body: payload
  });
}

export function passwordlessLogin(payload) {
  return request ({
    url: PASSWORDLESS_LOGIN_RETURN_URL,
    method: 'POST',
    body: payload
  });
}

export function loadSujetCollection(query) {
  return request ({
    url: `${HOST}/sujets?${qs.stringify(query, {
      arrayFormat: 'repeat'
    })}`
  });
}

export function loadSujetItem(sujetId) {
  return request ({
    url: `${HOST}/sujets/${sujetId}`
  });
}

export function updateSujetName({sujetId, version, name}) {
  return request ({
    url: `${HOST}/sujets/${sujetId}/name`,
    method: 'PUT',
    body: {
      name,
      version
    }
  });
}
export function deleteSujetItem({sujetId, version}) {
  return request ({
    url: `${HOST}/sujets/${sujetId}`,
    method: 'DELETE',
    body: {
      version
    }
  });
}
export function reportSujetValidationError(sujetId) {
  return request ({
    url: `${HOST}/sujets/${sujetId}/reports/errors/validation`,
    method: 'POST'
  });
}
export function replaceSourceFile({sujetId, filekey}) {
  return request ({
    url: `${HOST}/sujets/${sujetId}/sourcefile`,
    method: 'PUT',
    body: {
      filekey: filekey
    }
  });
}

export function loadBlockCollection(date) {
  return request ({
    url: `${HOST}/blocks?date=${date}`
  });
}

export function loadDcpCollection(date) {
  return request ({
    url: `${HOST}/dcps?date=${date}`
  });
}

export function renderBlockDcp(blockId) {
  return request ({
    url: `${HOST}/blocks/${blockId}/dcps`,
    method: 'POST'
  });
}

export function loadBlockItem(blockId) {
  return request ({
    url: `${HOST}/blocks/${blockId}`
  });
}

export function loadSegmentCollection(blockId) {
  return request ({
    url: `${HOST}/blocks/${blockId}/segments`
  });
}

export function updateReelOrder({blockId, segmentId, list, hash}) {
  return request ({
    url: `${HOST}/blocks/${blockId}/segments/${segmentId}/order`,
    method: 'PUT',
    body: {
      hash,
      list
    }
  });
}

export function loadScheduleCollection(sujetId, query) {
  return request ({
    url: `${HOST}/sujets/${sujetId}/schedule?${qs.stringify(query)}`
  });
}
export function toggleDateslotbooking({sujetId, stageIds, dateslotIds, query, book}) {
  let method = book ? 'PUT' : 'DELETE';

  return request ({
    url: `${HOST}/sujets/${sujetId}/booking?${qs.stringify(query)}`,
    method,
    body: {
      stages: stageIds,
      dateslots: dateslotIds
    }
  });
}

export const users = generic('users');
export const customers = generic('customers');
export const stages = generic('stages');
export const locations = generic('locations');
export const companies = generic('companies');
export const activities = generic('activities');

export function sendUserWelcomeMail(userId) {
  return request ({
    url: `${HOST}/users/${userId}/emails/welcome`,
    method: 'POST'
  });
}
export function findUserActivities(userId) {
  return request ({
    url: `${HOST}/users/${userId}/activities`
  });
}
export function editUserActivities(userId, activityId, add) {
  let method = add ? 'POST' : 'DELETE';

  return request ({
    url: `${HOST}/users/${userId}/activities/${activityId}`,
    method
  });
}

export function findCustomerReadAccess(customerId) {
  return request ({
    url: `${HOST}/customers/${customerId}/access/read/customers`
  });
}
export function editCustomerReadAccess(customerId, customerReadAccessId, add) {
  let method = add ? 'POST' : 'DELETE';

  return request ({
    url: `${HOST}/customers/${customerId}/access/read/customers/${customerReadAccessId}`,
    method
  });
}

export function findCustomerCompanySegment(customerId) {
  return request ({
    url: `${HOST}/customers/${customerId}/companies/write`
  });
}
export function editCustomerCompanySegment(customerId, companyId, add) {
  let method = add ? 'POST' : 'DELETE';

  return request ({
    url: `${HOST}/customers/${customerId}/companies/write/${companyId}`,
    method
  });
}

export function findCustomerReadCompanies(customerId) {
  return request ({
    url: `${HOST}/customers/${customerId}/companies/read`
  });
}
export function editCustomerReadCompany(customerId, companyId, add) {
  let method = add ? 'POST' : 'DELETE';

  return request ({
    url: `${HOST}/customers/${customerId}/companies/read/${companyId}`,
    method
  });
}

export function getSegmentOrder(companyId, hasAudio) {
  let muteParam = hasAudio ? 'audio' : 'mute';

  return request ({
    url: `${HOST}/companies/${companyId}/blockorder/${muteParam}`
  });
}
export function setSegmentOrder(companyId, hasAudio, order) {
  let muteParam = hasAudio ? 'audio' : 'mute';

  return request ({
    url: `${HOST}/companies/${companyId}/blockorder/${muteParam}`,
    method: 'PUT',
    body: order
  });
}

export function loadDateslotCollection(startdate, enddate) {
  return request ({
    url: `${HOST}/dateslots?startdate=${startdate}&enddate=${enddate}`
  });
}

export function passwordReset(payload) {
  return request ({
    url: PASSWORDRESET_URL,
    method: 'POST',
    body: payload
  });
}

export function passwordResetReturn(payload) {
  return request ({
    url: PASSWORDRESET_RETURN_URL,
    method: 'POST',
    body: payload
  });
}