import Vue from 'vue';
import queue from 'queue';
import {
  getUploadUrl,
  createSujet,
  uploadFile
} from '../assets/api';

const q = queue({
  concurrency: 3,
  autostart: true
});

q.start();

let idCounter = 0;


function initData() {
  return {
    list: [],
    entities: {}
  };
}

export default {
  state: {
    data: initData()
  },
  mutations: {
    createUpload (state, {newFile, id}) {
      let entities = {},
        upload;

      state.data.list.push(id);
      upload = {
        id,
        queued: false,
        started: false,
        finished: false,
        request: null,
        error: false,
        progress: 0,
        name: newFile.name,
        file: newFile.file,
        type: newFile.type,
        sujettype: newFile.sujettype,
        sound: true,
        duration: newFile.sujettype === 'dia' ? 7 : 0,
        sujet: null
      };

      entities[upload.id] = upload;
      state.data.entities = Object.assign({}, state.data.entities, entities);
    },
    setImage (state, {id, image}) {
      state.data.entities[id].image = image;
    },
    setDuration (state, {id, duration}) {
      state.data.entities[id].duration = Number(duration);
    },
    setName (state, {id, name}) {
      state.data.entities[id].name = name;
    },
    setSound (state, {id, sound}) {
      state.data.entities[id].sound = sound;
    },
    setSujet (state, {id, sujet}) {
      state.data.entities[id].sujet = sujet;
    },
    uploadUpdateProgress(state, {id, progress}) {
      state.data.entities[id].progress = progress;
    },
    uploadStarted (state, id) {
      state.data.entities[id].started = true;
    },
    uploadFinished (state, id) {
      state.data.entities[id].queued = false;
      state.data.entities[id].finished = true;
    },
    uploadError (state, {id, message}) {
      state.data.entities[id].error = message;
      state.data.entities[id].started = false;
      state.data.entities[id].queued = false;
      state.data.entities[id].request = null;
      state.data.entities[id].finished = true;
    },
    remove (state, id) {
      state.data.list.splice(state.data.list.indexOf(id), 1);
      Vue.delete(state.data.entities, id);
    },
    setRequest(state, {id, request}) {
      state.data.entities[id].request = request;
    },
    enqueue(state, id) {
      state.data.entities[id].queued = true;
    },
    dequeue(state, id) {
      state.data.entities[id].queued = false;
    },
    cancel(state, id) {
      state.data.entities[id].started = false;
      state.data.entities[id].queued = false;
    },
    reset (state) {
      state.data = initData();
    }
  },
  actions: {
    createUpload(context, file) {
      let nextId = idCounter++;

      context.commit('createUpload', {
        newFile: file, 
        id: nextId
      });
    },
    enqueueOne(context, id) {
      if (context.getters.enqueable(id)) {
        context.commit('enqueue', id);
        q.push(function(cb){
          let uploadStillExists = context.getters.getUploadById(id),
            uploadIsStillEnqueued = context.getters.enqueued(id),
            noRunningRequestForThisUpload = !context.getters.getUploadById(id).request;

          if (uploadStillExists && uploadIsStillEnqueued && noRunningRequestForThisUpload) {
            let upload = context.getters.getUploadById(id);

            context.commit('uploadStarted', id);
            getUploadUrl(upload.file.name)
              .then(({key, url, contentType}) => {
                let sujettype = upload.sujettype,
                  request;
                
                if (sujettype === 'spot') {
                  if (upload.sound) {
                    sujettype = 'spotsound';
                  } else {
                    sujettype = 'spotmute';
                  }
                } 

                request = uploadFile({
                  url,
                  file: upload.file,
                  contentType
                }, function progress(progress) {
                  if (context.getters.getUploadById(id) && context.getters.enqueued(id)) {
                    context.commit('uploadUpdateProgress', {
                      id,
                      progress
                    });
                  }
                }, function finished(error){
                  if (error) {
                    if (context.getters.getUploadById(id) && context.getters.enqueued(id)) {
                      context.commit('uploadError', {
                        id,
                        message: error.message
                      });
                    }
                    cb();
                  } else {
                    createSujet({
                      name: upload.name,
                      filekey: key,
                      sujettype,
                      duration: upload.duration
                    }).then(() => {
                      context.commit('remove', id);
                    }).catch((error) => {
                      if (context.getters.getUploadById(id) && context.getters.enqueued(id)) {
                        context.commit('uploadError', {
                          id,
                          message: error.message
                        });
                      }
                    });
                    cb();
                  }
                });

                context.commit('setRequest', {id, request});
              })
              .catch((error) => {
                context.commit('uploadError', {
                  id,
                  message: error.message
                });
              });
          } else {
            cb();
          }
        });
      }
    },
    enqeueAll(context) {
      context.getters.listUploads.forEach((upload) => {
        context.dispatch('enqueueOne', upload.id);
      });
    },
    dequeueOne(context, id) {
      if (context.getters.dequeable(id)) {
        context.commit('dequeue', id);
      }
    },
    deqeueAll(context) {
      context.getters.listUploads.forEach((upload) => {
        context.dispatch('dequeueOne', upload.id);
      });
    },
    abortOne(context, id) {
      if (context.getters.abortable(id)) {
        context.state.data.entities[id].request.abort();
      }
    },
    abortAll(context) {
      context.getters.listUploads.forEach((upload) => {
        context.dispatch('abortOne', upload.id);
      });
    },
    removeOne(context, id) {
      if (context.getters.removable(id)) {
        context.commit('remove', id);
      }
    },
    removeAll(context) {
      context.getters.listUploads.forEach((upload) => {
        context.dispatch('removeOne', upload.id);
      });
    },
    setDuration(context, {id, duration}) {
      if (context.getters.editable(id)) {
        context.commit('setDuration', {id, duration});
      }
    },
    setName(context, {id, name}) {
      if (context.getters.editable(id)) {
        context.commit('setName', {id, name});
      }
    },
    setSound(context, {id, sound}) {
      if (context.getters.editable(id)) {
        context.commit('setSound', {id, sound});
      }
    }
  },
  getters: {
    editable: (state, getters) => (id) => {
      let upload = getters.getUploadById(id);
      return upload.file && !upload.queued && !upload.started && !upload.error && !upload.finished;
    },
    dequeable: (state, getters) => (id) => {
      let upload = getters.getUploadById(id);
      return upload.queued && !upload.started;
    },
    enqueable: (state, getters) => (id) => {
      let upload = getters.getUploadById(id);
      return upload.file && !upload.queued && !upload.finished;
    },
    enqueued: (state, getters) => (id) => {
      let upload = getters.getUploadById(id);
      return upload.queued;
    },
    abortable: (state, getters) => (id) => {
      let upload = getters.getUploadById(id);
      return upload.started && !upload.finished && !upload.error && upload.request;
    },
    removable: (state, getters) => (id) => {
      let upload = getters.getUploadById(id);
      return !upload.queued || (!upload.queued && !upload.started) || upload.finished;
    },
    listUploads: (state) => {
      return state.data.list.map(uploadId => {
        return state.data.entities[uploadId];
      }).reverse();
    },
    getUploadById: (state) => (id) => {
      if (state.data.entities[id]) {
        return state.data.entities[id];
      } else {
        return false;
      }
    },
    totalUploads: (state) => {
      return state.data.list.length;
    },
    pendingUploads: (state) => {
      return state.data.list.map(uploadId => {
        return state.data.entities[uploadId];
      }).reduce((accumulator, upload) => {
        return accumulator || upload.queued || upload.started || upload.request;
      }, false);
    },
    startableUploads: (state) => {
      return state.data.list.map(uploadId => {
        return state.data.entities[uploadId];
      }).reduce((accumulator, upload) => {
        return accumulator || (upload.file && !(upload.queued || upload.started || upload.request));
      }, false);
    }
  }
};