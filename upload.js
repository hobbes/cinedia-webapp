require('dotenv').config();

var FtpDeploy = require('ftp-deploy'),
  path = require('path'),
  ftpDeploy = new FtpDeploy();

var config = {
  user: process.env.FTP_USER,
  password: process.env.FTP_PASSWORD,
  host: process.env.FTP_HOST,
  port: 21,
  localRoot: path.join(__dirname, '/dist'),
  remoteRoot: '/',
  include: ['*', '.*'],
  exclude: ['.DS_Store', '.ftpquota']
  // deleteRemote: true
};
  
ftpDeploy.deploy(config)
  .then(() => console.log('finished'))
  .catch(err => console.log('error:', err));

ftpDeploy.on('uploaded', function(data) {
  console.log('file:', data.filename);
});
ftpDeploy.on('upload-error', function (data) {
  console.log(data.err);
});